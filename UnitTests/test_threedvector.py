import unittest
from threedvector import threedvector


class Test_TestThreeDVector(unittest.TestCase):

    def test_to_cartesian_return_valid_results(self):
        # https://keisan.casio.com/exec/system/1359534351
        s_vector = threedvector.Vector(5, 60, 30)
        result = s_vector.cartesian()
        self.assertEqual(round(result[0], 5), 1.25)
        self.assertEqual(round(result[1], 5), 2.16506)
        self.assertEqual(round(result[2], 5), 4.33013)

    def test_to_cartesian_return_valid_results_with_large_tehta_phi(self):
        # https://keisan.casio.com/exec/system/1359534351
        s_vector = threedvector.Vector(-5, 560, -9830)
        result = s_vector.cartesian()
        self.assertEqual(round(result[0], 5), -4.41511)
        self.assertEqual(round(result[1], 5), -1.60697)
        self.assertEqual(round(result[2], 5), 1.71010)

    def test_mult_returns_valid_results_with_any_scalar(self):
        s_vector = threedvector.Vector(1, 4, 5)
        m_vector = 3.1*s_vector*(-3)
        self.assertEqual(round(m_vector.magnitude(), 5), -9.3)

    def test_mult_raises_TypeError_when_mulitplied_by_non_numerical(self):
        s_vector = threedvector.Vector(1, 4, 5)
        t_string = "non number"
        with self.assertRaises(TypeError):
            s_vector*t_string

    def test_add_returns_valid_results(self):
        vector1 = threedvector.Vector(1, 2, 3, coords="cartesian")
        vector2 = threedvector.Vector(1, 2, 3, coords="cartesian")
        result = (vector1 + vector2).cartesian()
        self.assertEqual(round(result[0], 5), 2)
        self.assertEqual(round(result[1], 5), 4)
        self.assertEqual(round(result[2], 5), 6)

    def test_sub_returns_valid_results(self):
        vector1 = threedvector.Vector(10, 20, 30, coords="cartesian")
        vector2 = threedvector.Vector(1, 2, 3, coords="cartesian")
        result = (vector1 - vector2).cartesian()
        self.assertEqual(round(result[0], 5), 9)
        self.assertEqual(round(result[1], 5), 18)
        self.assertEqual(round(result[2], 5), 27)

    def test_eq_returns_valid_restuls(self):
        vector1 = threedvector.Vector(2, 3, 4)
        vector2 = threedvector.Vector(2, 20, 30)
        self.assertTrue(vector1 == vector2)
        self.assertTrue(not(vector1 < vector2))
        self.assertTrue(not(vector1 > vector2))

    def test_gt_returns_valid_restuls(self):
        vector1 = threedvector.Vector(2, 3, 4)
        vector2 = threedvector.Vector(1, 20, 30)
        self.assertTrue(vector1 > vector2)
        self.assertTrue(not(vector1 == vector2))
        self.assertTrue(not(vector1 < vector2))

    def test_ge_returns_valid_restuls(self):
        vector1 = threedvector.Vector(2, 3, 4)
        vector2 = threedvector.Vector(2, 20, 30)
        vector3 = threedvector.Vector(3, 20, 30)
        self.assertTrue(vector1 >= vector2)
        self.assertTrue(vector1 == vector2)
        self.assertTrue(not(vector1 < vector2))
        self.assertTrue(vector3 >= vector2)
        self.assertTrue(not(vector3 == vector2))
        self.assertTrue(not(vector3 < vector2))

    def test_str_prints_correct_string(self):
        vector = threedvector.Vector(1, 2, 3)
        result = vector.__str__()
        self.assertEqual(result, "(1, 2, 3)")

    def test_is_same_returns_true_for_similar_vectors(self):
        vector1 = threedvector.Vector(2, 3, 4)
        vector2 = threedvector.Vector(2, 3, 4)
        self.assertTrue(vector1.is_same(vector2))

    def test_is_same_returns_false_for_dissimilar_vectors(self):
        vector1 = threedvector.Vector(2, 3, 4)
        vector2 = threedvector.Vector(2, 3, 3)
        vector3 = threedvector.Vector(2, 4, 4)
        vector4 = threedvector.Vector(3, 3, 4)
        self.assertFalse(vector1.is_same(vector2))
        self.assertFalse(vector1.is_same(vector3))
        self.assertFalse(vector1.is_same(vector4))

    def test_unit_returns_valid_results(self):
        # https://www.symbolab.com/solver/vector-unit-calculator
        s_vector = threedvector.Vector(2.4, -4.6, -3.2, coords="cartesian")
        result = s_vector.unit().cartesian()
        self.assertEqual(round(result[0], 5), 0.39371)
        self.assertEqual(round(result[1], 5), -0.75461)
        self.assertEqual(round(result[2], 5), -0.52494)

    def test_unit_raises_ZeroDivisionError_with_zero_length_vector(self):
        # https://www.symbolab.com/solver/vector-unit-calculator
        s_vector = threedvector.Vector(0, -4.6, -3.2)
        with self.assertRaises(ZeroDivisionError):
            s_vector.unit()

    def test_cross_returns_valid_results(self):
        # https://www.wolframalpha.com/input/?i=cross+product+vectors&assumption=%7B%22F%22,+%22CrossProduct%22,+%22crossVector1%22%7D+-%3E%22%7B1.1,+20.3,+3.1%7D%22&assumption=%7B%22F%22,+%22CrossProduct%22,+%22crossVector2%22%7D+-%3E%22%7B3.23,+4.32,+5.45%7D%22
        vector1 = threedvector.Vector(1.1, 20.3, 3.1, coords="cartesian")
        vector2 = threedvector.Vector(3.23, 4.32, 5.45, coords="cartesian")
        result = vector1.cross(vector2).cartesian()
        self.assertEqual(round(result[0], 3), 97.243)
        self.assertEqual(round(result[1], 3), 4.018)
        self.assertEqual(round(result[2], 3), -60.817)

    def test_cross_returns_zero_vector_with_one_zero_vector_input(self):
        # https://www.wolframalpha.com/input/?i=cross+product+vectors&assumption=%7B%22F%22,+%22CrossProduct%22,+%22crossVector1%22%7D+-%3E%22%7B0,+0,+0%7D%22&assumption=%7B%22F%22,+%22CrossProduct%22,+%22crossVector2%22%7D+-%3E%22%7B-3.23,+-4.32,+-5.45%7D%22
        vector1 = threedvector.Vector(1.1, 20.3, 3.1, coords="cartesian")
        vector2 = threedvector.Vector(0, 0, 0, coords="cartesian")
        result = vector1.cross(vector2).cartesian()
        self.assertEqual(round(result[0], 3), 0)
        self.assertEqual(round(result[1], 3), 0)
        self.assertEqual(round(result[2], 3), 0)

    def test_dot_returns_valid_results(self):
        # https://www.wolframalpha.com/input/?i=dot+product+vectors&assumption=%7B%22F%22,+%22DotProduct%22,+%22dotVector1%22%7D+-%3E%22%7B1.1,+20.3,+3.1%7D%22&assumption=%7B%22F%22,+%22DotProduct%22,+%22dotVector2%22%7D+-%3E%22%7B3.23,+4.32,+5.45%7D%22
        vector1 = threedvector.Vector(1.1, 20.3, 3.1, coords="cartesian")
        vector2 = threedvector.Vector(3.23, 4.32, 5.45, coords="cartesian")
        result = vector1.dot(vector2)
        self.assertEqual(round(result, 3), 108.144)

    def test_dot_returns_zero_with_zero_vector_input(self):
        # https://www.wolframalpha.com/input/?i=dot+product+vectors&assumption=%7B%22F%22,+%22DotProduct%22,+%22dotVector1%22%7D+-%3E%22%7B1.1,+20.3,+3.1%7D%22&assumption=%7B%22F%22,+%22DotProduct%22,+%22dotVector2%22%7D+-%3E%22%7B3.23,+4.32,+5.45%7D%22
        vector1 = threedvector.Vector(0, 0, 0, coords="cartesian")
        vector2 = threedvector.Vector(3.23, 4.32, 5.45, coords="cartesian")
        result = vector1.dot(vector2)
        self.assertEqual(round(result, 3), 0)

    def test_angle_returns_valid_results(self):
        # https://www.symbolab.com/solver/vector-angle-calculator/angle%20%5Cbegin%7Bpmatrix%7D-1.1%2620.3%263.1%5Cend%7Bpmatrix%7D%2C%20%5Cbegin%7Bpmatrix%7D3.23%26-4.32%265.45%5Cend%7Bpmatrix%7D
        vector1 = threedvector.Vector(-1.1, 20.3, 3.1, coords="cartesian")
        vector2 = threedvector.Vector(3.23, -4.32, 5.45, coords="cartesian")
        result = vector1.angle(vector2)
        self.assertEqual(round(result, 5), 118.13299)

    def test_angle_returns_zero_if_vectors_are_same(self):
        vector1 = threedvector.Vector(3.23, -4.32, 5.45, coords="cartesian")
        vector2 = threedvector.Vector(3.23, -4.32, 5.45, coords="cartesian")
        result = vector1.angle(vector2)
        self.assertEqual(round(result, 5), 0)

    def test_angle_returns_zero_if_one_has_no_length(self):
        vector1 = threedvector.Vector(0, -4.32, 5.45)
        vector2 = threedvector.Vector(3.23, -4.32, 5.45, coords="cartesian")
        result = vector1.angle(vector2)
        self.assertEqual(round(result, 5), 0)

    def test_magnitude_returns_valid_result(self):
        vector = threedvector.Vector(34.1, 67, 89)
        result = vector.magnitude()
        self.assertEqual(round(result, 5), 34.1)

    def test_theta_returns_valid_result(self):
        vector = threedvector.Vector(34, 67.1, 89)
        result = vector.theta()
        self.assertEqual(round(result, 5), 67.1)

    def test_phi_returns_valid_result(self):
        vector = threedvector.Vector(34, 67, 89.1)
        result = vector.phi()
        self.assertEqual(round(result, 5), 89.1)

    def test_set_magnitude_returns_valid_results(self):
        vector = threedvector.Vector(34, 67, 89.1)
        vector.set_magnitude(1)
        self.assertEqual(vector.magnitude(), 1)
        self.assertEqual(vector.theta(), 67)
        self.assertEqual(vector.phi(), 89.1)

    def test_set_theta_returns_valid_results(self):
        vector = threedvector.Vector(34, 67, 89.1)
        vector.set_theta(1)
        self.assertEqual(vector.magnitude(), 34)
        self.assertEqual(vector.theta(), 1)
        self.assertEqual(vector.phi(), 89.1)

    def test_set_phi_returns_valid_results(self):
        vector = threedvector.Vector(34, 67, 89.1)
        vector.set_phi(1)
        self.assertEqual(vector.magnitude(), 34)
        self.assertEqual(vector.theta(), 67)
        self.assertEqual(vector.phi(), 1)

    def test_copy_returns_valid_results(self):
        vector1 = threedvector.Vector(23, 45, 67)
        vector2 = threedvector.copy(vector1)
        self.assertEqual(vector1.magnitude(), vector2.magnitude())
        self.assertEqual(vector1.theta(), vector2.theta())
        self.assertEqual(vector1.phi(), vector1.phi())

    def test_copy_does_not_alter_copied_attributes(self):
        vector1 = threedvector.Vector(23, 45, 67)
        vector2 = threedvector.copy(vector1)
        vector2.set_magnitude(vector2.magnitude()*3)
        vector2.set_theta(vector2.theta()*3)
        vector2.set_phi(vector2.phi()*3)
        self.assertEqual(vector1.magnitude(), 23)
        self.assertEqual(vector1.theta(), 45)
        self.assertEqual(vector1.phi(), 67)


if __name__ == '__main__':
    unittest.main()
